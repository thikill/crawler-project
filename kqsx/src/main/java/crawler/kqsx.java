package crawler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Hello world!
 *
 */
public class kqsx {
	public static void main(String[] args) {
		try {
			long startTime = System.currentTimeMillis();
			Document document = Jsoup.connect("http://ketqua.net/xo-so-mien-bac.php")
					.userAgent(
							"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36")
					.timeout(180000).get();
			Element divKQ = document.select("#ketqua_mb").first();

			File file = new File("kq.txt");
			if (file.exists() == true) {
				file.delete();
			}
			file.createNewFile();

			try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("kq.txt", true)))) {
				Element tblKQ = divKQ.select("div>table>tbody").first();
				Elements rows = tblKQ.select("tr");
				for (Element row : rows) {
					Elements tds = row.select("td");
					tds.remove(0);
					for (int i = 0; i < tds.size(); i++) {
						if (i < tds.size() - 1) {
							out.print(tds.get(i).text() + ",");
						} else {
							out.print(tds.get(i).text());
						}

					}
					out.println("");
				}

			} catch (IOException e) {
				throw e;
			}

			long endTile = System.currentTimeMillis();
			System.out.println("total time = " + ((endTile - startTime) / 1000));

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
